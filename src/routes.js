import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
import Commenting from './views/detail/Comment.vue'
import Topic from './views/detail/Topic.vue'
import Reply from './views/detail/Reply.vue'
import Sort from './views/detail/Sort.vue'

let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    {
        path: '/',
        component: Home,
        name: '首页',
        iconCls: 'fa fa-home',//图标样式class
        leaf: true,//只有一个节点
        children: [
            { path: '/main', component: Main, name: '首页', hidden: true }
        ]
    },
    {
        path: '/comment',
        component: Home,
        name: '评论管理',
        iconCls: 'fa fa-commenting',
        leaf: true,//只有一个节点
        children: [
            { path: '/comment', component:Commenting, name: '评论管理' }
        ]
    },
    {
        path: '/topic',
        component: Home,
        name: '我的',
        iconCls: 'fa fa-user',
        children: [
            { path: '/topic', component: Topic, name: '课题管理' },
            { path: '/reply', component: Reply, name: '回复评论' }
        ]
    },
    {
        path: '/sort',
        component: Home,
        name: '',
        iconCls: 'fa fa-th',
        leaf: true,//只有一个节点
        children: [
            { path: '/sort', component: Sort, name: '分类管理' }
        ]
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;